> 前几天在layui官网看到官网停止服务的公告，瞬间感觉爷青结，刚工作的时候就在用layui，好在贤心大大并没有放弃。

> 最近看到有朋友在Fork我的layfilter项目，于是产生了重构的想法
时隔两年，layFilter(原layfiter已更名)重新回归

+ 重新设计样式，原来的table布局改为div ```flex```布局
+ 升级layui版本到2.6.8（未重度依赖layui，可以放心升级）
+ 增加颜色设置
+ 增加重置方法
+ 增加值改变监听函数onChange
+ 增加success监听，在渲染完成后可以调用该方法进行回显赋值
+ 优化重置方法
+ 去掉原来的url请求接口的方式获取dataSource

[![45S4pT.md.png](https://z3.ax1x.com/2021/09/29/45S4pT.md.png)](https://imgtu.com/i/45S4pT)


## 使用方法

克隆项目后，整合到自己的项目，需自行修改文件路径

```javascript
layui.config({
    base: '../layui_exts/' //配置 layui 第三方扩展组件存放的基础目录
  }).extend({
    layFilter:'layFilter/layFilter'
  });


  function getUrlParam(url,name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = url.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}
```

